package br.com.devmedia.service;

import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.devmedia.dao.FornecedorDao;
import br.com.devmedia.domain.Fornecedor;

@Service
@Transactional
public class FornecedorServiceImpl implements FornecedorService  {
	
	@Autowired
	private FornecedorDao fornecedorDao;
	
	@Override
	public void salvar(Fornecedor fornecedor) {
		fornecedorDao.salvar(fornecedor);
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Fornecedor> recuperar() {
		// TODO Auto-generated method stub
		return fornecedorDao.recuperar();
	}

	@Override
	@Transactional(readOnly = true)
	public Fornecedor recuperarPorId(long id) {
		// TODO Auto-generated method stub
		return fornecedorDao.recuperarPorID(id);
	}

	@Override
	public void atualizar(Fornecedor fornecedor) {
		fornecedorDao.atualizar(fornecedor);
		
	}

	@Override
	public void excluir(long id) {
		fornecedorDao.excluir(id);
		
	}

}
