package br.com.devmedia.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.devmedia.dao.PedidoDao;
import br.com.devmedia.domain.Pedido;

@Service
@Transactional
public class PedidoServiceImpl implements PedidoService {
	
	@Autowired
	private PedidoDao pedidoDao;// Injetando o objeto nesse atributo
	
	@Autowired
	private FornecedorService fornecedorService;
	
	@Override
	public void salvar(Pedido pedido, long fornecedorId) {
		pedido.setFornecedor(fornecedorService.recuperarPorId(fornecedorId));
		pedidoDao.salvar(pedido);
	}

	@Override
	@Transactional(readOnly = true)//
	public List<Pedido> recuperarPorFornecedor(long fornecedorId) {
		
		return pedidoDao.recuperarPorFornecedor(fornecedorId);
	}
	@Transactional(readOnly = true)
	@Override
	public Pedido recuperarPorFornecedorIdEPedidoId(long fornecedorId, long pedidoId) {
		return pedidoDao.recuperarPorFornecedorEPedidoId(fornecedorId, pedidoId);
	}

	@Override
	public void atualizar(Pedido pedido, long fornecedorId) {
		pedido.setFornecedor(fornecedorService.recuperarPorId(fornecedorId));
		pedidoDao.atualizar(pedido);
	}

	@Override
	public void excluir(long fornecedorId, long pedidoId) {
		pedidoDao.excluir(recuperarPorFornecedorIdEPedidoId(fornecedorId, pedidoId).getCodigoPedido());
	}
	
}
