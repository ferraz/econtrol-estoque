package br.com.devmedia.service;

import java.util.List;

import br.com.devmedia.domain.Pedido;

public interface PedidoService {
	
	void salvar(Pedido pedido, long fornecedorId);
	List<Pedido> recuperarPorFornecedor(long fornecedorId);
	Pedido recuperarPorFornecedorIdEPedidoId(long fornecedorId, long pedido);
	void atualizar(Pedido pedido, long fornecedorId);
	void excluir(long fornecedorId, long pedidoId);
}
