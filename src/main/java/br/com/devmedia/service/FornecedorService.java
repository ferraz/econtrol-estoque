package br.com.devmedia.service;

import java.util.List;

import br.com.devmedia.domain.Fornecedor;

public interface FornecedorService {
	
	void salvar(Fornecedor fornecedor);
	List<Fornecedor> recuperar();
	Fornecedor recuperarPorId(long id);
	void atualizar(Fornecedor fornecedor);
	void excluir(long id);
}
