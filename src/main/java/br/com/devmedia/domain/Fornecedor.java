package br.com.devmedia.domain;
// Generated 14/09/2018 19:17:15 by Hibernate Tools 4.0.1.Final


import java.util.List;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;


@Entity
@Table(name = "fornecedores")
public class Fornecedor  {
	
	@Id
	@Column(name = "codigoFornecedor", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long codigoFornecedor;
	
	@NotBlank
	@Column(nullable = false, length = 60)
	@Size(min = 2, max = 60)
	private String nome;
	
	@NotBlank
	@Column(nullable = false)
	private String endereco;
	
	@NotBlank
	@Column(length = 15, nullable = false)
	private String telefone;
	
	
	@OneToMany(mappedBy = "fornecedor", cascade = CascadeType.ALL)
	private List<Pedido> pedidos;

	public Fornecedor() {
	}


	public long getCodigoFornecedor() {
		return codigoFornecedor;
	}

	public void setCodigoFornecedor(long codigoFornecedor) {
		this.codigoFornecedor = codigoFornecedor;
	}


	public void setCodigoFornecedor(int codigoFornecedor) {
		this.codigoFornecedor = codigoFornecedor;
	}



	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Pedido> getPedidos() {
		return pedidos;
	}

	public void setPedidos(List<Pedido> pedidos) {
		this.pedidos = pedidos;
	}
	
	

	
	

	
	

}
