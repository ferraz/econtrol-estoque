package br.com.devmedia.domain;


import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.devmedia.domain.Fornecedor;

@Entity
@Table(name = "pedido")
public class Pedido {
	
	
	@Id
	@Column(name = "codigoPedido", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int codigoPedido;
	
	@Column(nullable = false)
    private String descricao;
	
	@ManyToOne
	@JoinColumn(name = "id_fornecedor_fk")
	private Fornecedor fornecedor;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "dataPedido", length = 0)
	private Date dataPedido;
	
	@Column(name = "valorPedido", precision = 12, scale = 0)
	private Float valorPedido;
	
	//private Set itempedidos = new HashSet(0);

	public Pedido() {
	}

	public int getCodigoPedido() {
		return codigoPedido;
	}

	public void setCodigoPedido(int codigoPedido) {
		this.codigoPedido = codigoPedido;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public Date getDataPedido() {
		return dataPedido;
	}

	public void setDataPedido(Date dataPedido) {
		this.dataPedido = dataPedido;
	}

	public Float getValorPedido() {
		return valorPedido;
	}

	public void setValorPedido(Float valorPedido) {
		this.valorPedido = valorPedido;
	}

	//@OneToMany(fetch = FetchType.LAZY, mappedBy = "pedido")
	
	
}
