package br.com.devmedia.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.devmedia.domain.Fornecedor;
import br.com.devmedia.service.FornecedorService;

@Controller
@RequestMapping("fornecedores")
public class FornecedorController {
	
	@Autowired
	private FornecedorService fornecedorService; // Fazendo a injeção  da dependência no momento em que o controller for criado.

	/*
     * ModelMap representa a implementação de um map feita pelo spring para que possamos
     * enviar dados para view
     * 
     * */
	
	@GetMapping("/listar")
	public ModelAndView listar(ModelMap model) {
		
		//Enviando os dados para view
		model.addAttribute("fornecedores", fornecedorService.recuperar());
		/*
		 * A partir do retorno desse ModelAndView o Front Controller do Spring MVC repassará essas informações para a view, 
		 * que processará a página list.html com os dados enviados no model e renderizará o HTML,
		 * o devolvendo para o Front Controller enviar a resposta para o browser.
		 * 
		 * */
		return new ModelAndView("/fornecedor/list", model);
	}
	
	@GetMapping("/cadastro")
	public String preSalvar(@ModelAttribute("fornecedor") Fornecedor fonecedor) {
		return "/fornecedor/add";
	}
	
	
	/*@Valid estamos indicando que essas regras devem ser validadas neste momento.
	 * 
	 * 
	 * */
	@PostMapping("/salvar")
	public String salvar(@Valid @ModelAttribute("fornecedor")
		Fornecedor fornecedor, BindingResult result, RedirectAttributes attr) {
			
		if (result.hasErrors()) {
			return "/fornecedor/add";
		}
		
		fornecedorService.salvar(fornecedor);
		attr.addFlashAttribute("mensagem" , "Fornecedor criado com sucesso!");
		return "redirect:/fornecedores/listar";
	}
	
	
	//@PathVariable envia id passado por parametro na a anotação @GetMapping para argumento id, no momento em que o método é chamado.
	@GetMapping("/{id}/atualizar")
	public ModelAndView preAtualizar(@PathVariable("id") 
		long id, ModelMap model) {
		Fornecedor fornecedor = fornecedorService.recuperarPorId(id);
		model.addAttribute("fornecedor", fornecedor);
		return new ModelAndView("/fornecedor/add", model);
	}
	
	@PutMapping("/salvar")
	public String atualizar(@Valid @ModelAttribute("fornecedor") 
		Fornecedor fornecedor, BindingResult result, RedirectAttributes attr) {
		if (result.hasErrors()) {
			return "/fornecedor/add";
		}
		
		fornecedorService.atualizar(fornecedor);
		attr.addFlashAttribute("mensagem", "Fornecedor Atualizado com sucesso.");
		return "redirect:/fornecedores/listar";
	}
	
	@GetMapping("/{id}/remover")
	public String remover(@PathVariable("id") long id, RedirectAttributes attr) {
		fornecedorService.excluir(id);
		attr.addFlashAttribute("mensagem", "Fornecedor excluido com sucesso.");
		return "redirect:/fornecedores/listar";
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
