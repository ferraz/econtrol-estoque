package br.com.devmedia.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import br.com.devmedia.domain.Pedido;

@Repository
public class PedidoDaoImpl implements PedidoDao {
	
	private EntityManager em;
	
	@Override
	public void salvar(Pedido pedido) {
		
		em.persist(pedido);
	}

	@Override
	public List<Pedido> recuperarPorFornecedor(long fornecedorId) {
		// TODO Auto-generated method stub
		return em.createQuery("select p from Pedido m where m.fornecedor.id = :fornecedorId", Pedido.class)
			.setParameter("fornecedorId", fornecedorId)
			.getResultList();
	}

	@Override
	public Pedido recuperarPorFornecedorEPedidoId(long fornecedorId, long pedidoId) {
		// TODO Auto-generated method stub
		return em.createQuery("select p from Pedido p where p.fornecedor.id = :fornecedorId and p.id = :pedidoId", Pedido.class)
				.setParameter("fornecedorId", fornecedorId)
				.setParameter("pedido", pedidoId)
				.getSingleResult();
	}

	@Override
	public void atualizar(Pedido pedido) {
		em.merge(pedido);
		
	}

	@Override
	public void excluir(long pedidoId) {
		em.remove(em.getReference(Pedido.class, pedidoId));
		
	}
}
