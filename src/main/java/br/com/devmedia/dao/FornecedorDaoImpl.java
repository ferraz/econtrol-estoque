package br.com.devmedia.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.com.devmedia.domain.Fornecedor;

@Repository
public class FornecedorDaoImpl implements FornecedorDao {
	
	@PersistenceContext
	private EntityManager em;
	
	@Override
	public void salvar(Fornecedor fornecedor) {
		em.persist(fornecedor);
	}

	@Override
	public List<Fornecedor> recuperar() {
		return em.createQuery("select f from Fornecedor f", Fornecedor.class).getResultList();
	}

	@Override
	public Fornecedor recuperarPorID(long id) {
		return em.find(Fornecedor.class, id);
	}

	@Override
	public void atualizar(Fornecedor fornecedor) {
		em.merge(fornecedor);
		
	}

	@Override
	public void excluir(long id) {
		em.remove(em.getReference(Fornecedor.class, id));
		
	}

}
