package br.com.devmedia.dao;

import java.util.List;

import br.com.devmedia.domain.Fornecedor;

public interface FornecedorDao {
	
	void salvar(Fornecedor fornecedor);
	List<Fornecedor> recuperar();
	Fornecedor recuperarPorID(long id);
	void atualizar(Fornecedor fornecedor);
	void excluir(long id);
	
}
