package br.com.devmedia.dao;

import java.util.List;

import br.com.devmedia.domain.Pedido;

public interface PedidoDao {
	
	void salvar(Pedido pedido);
	List<Pedido> recuperarPorFornecedor(long fornecedorId);
	Pedido recuperarPorFornecedorEPedidoId(long fornecedorId, long pedidoId);
	void atualizar(Pedido pedido);
	void excluir(long pedidoId);
}
